package com.sda.shopping;

public class Product {
    private String name;
    private int price;

    public Product(String name, int price) {
        this.name = name;
        this.price = price;
    }

    public String show() {
        return name + " avand pretul " + price;
    }

    public int getPrice() {
        return price;
    }
}
