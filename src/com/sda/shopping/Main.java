package com.sda.shopping;

import java.sql.PreparedStatement;
import java.util.Properties;
import java.util.Scanner;
import java.util.regex.Pattern;

public class Main {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {


        int nrOfProductsInBasket = 0;
        Product[] arrayProduse = new Product[5];
        Product product1 = new Product("Water", 5);
        arrayProduse[0] = product1;
        arrayProduse[1] = new Product("Soda", 10);
        arrayProduse[2] = new Product("Chips", 8);
        arrayProduse[3] = new Product("Candy", 3);
        arrayProduse[4] = new Product("Gum", 2);
        showMenu(arrayProduse);

        Product[] cart = addToCart(arrayProduse);
        System.out.println("Afisam produsele din cart");
        displayCart(cart);
        System.out.println(cartPriceTotal(cart));
        int totalPrice = cartPriceTotal(cart);
        payCart(totalPrice);
    }


    public static void showMenu(Product[] produse) {
        System.out.println("Type the nr for the product that you want:\n");

        for (int i = 0; i < produse.length; i++) {
            System.out.println((i) + " for: " + produse[i].show());
        }
    }

    public static void displayCart(Product[] cart) {

        for (int i = 0; i < cart.length; i++) {
            System.out.println(cart[i].show());
        }
    }

    public static int cartPriceTotal(Product[] cart) {
        int sum = 0;
        for (int i = 0; i < cart.length; i++) {
            sum += cart[i].getPrice();

        }
        return sum;
    }

    /**
     * Adaugam produsele in cos si facem suma produselor din cos.
     *
     * @return suma preturilor produselor din cos.
     */
    public static Product[] addToCart(Product[] arrayProduse) {

        Product[] cart = new Product[5];
        int i = 0;
        int optiune = 0;
        while (i < cart.length) {
            optiune = scanner.nextInt();
            if (optiune < 0 || optiune >= arrayProduse.length) {
                System.out.println("Optiune invalida");
                continue;
            }
            cart[i] = arrayProduse[optiune];
            i++;
        }
        return cart;
    }

    /**
     * Requesting cartPrice total from user
     *
     * @param cartPriceTotal amount to be paid by user
     */
    public static void payCart(int cartPriceTotal) {
        int userInput = scanner.nextInt();
        while (userInput != cartPriceTotal) {
            System.out.println("Valoare gresita, mai incearca!");
            userInput = scanner.nextInt();
        }
        System.out.println("Ati achitat cosul. Multumim!");
    }
}




