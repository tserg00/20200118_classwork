package com.sda.examples;

/**
 * java doc deasupra clasei
 * detaliem putin la ce e folosita clasa asta.
 * @param n = name of the student
 * @param c = CNP of the student
 * @param age = age of the student
 */
public class Student {
    private String name;
    private String CNP;
    private int age;

    public Student(String n, String c, int a) {
        this.name = n;
        this.CNP = c;
        this.age = a;
    }

    public Student() {

    }


    public void main(String[] args) {
        Student student = null;
//        Student student = "George";
        student.study();
        student.equals("John");

    }

    public void study() {
        System.out.println(name + " is studying" + " and has CNP " + CNP);
    }

    public void displayStudent() {
        System.out.println("Student name is " + name + " .CNP-ul este " + CNP);
    }

    public static void staticMethod() {
        System.out.println("Hello from static method");
        // showProfile();
    }

    public void showProfile() {
        System.out.println("Name of the student is " + name + "\nCNP of the student is" + CNP);
        System.out.println("Age is " + age);
        staticMethod();
    }
}
