package com.sda.examples;

public class main {
    public static void main(String[] args) {
        Student student = new Student();
//        student.equals("John");
//        Student student2 = null;
//        System.out.println(student);
//
//        student2.study();
//         System.out.println(student2);
//        Student student1 = new Student();
////        student1.name = "John";
////        student1.CNP = "123";
        Student student1 = new Student("George", "19099009099009", 22);
        student1.study();
        student1.displayStudent();
        Student student2 = new Student("Andrei", "123123123123", 25);
        student2.study();
        student2.displayStudent();
        student1.showProfile();
        student2.showProfile();
        student1.showProfile();
        student1.staticMethod();
        Student.staticMethod();

    }

}
